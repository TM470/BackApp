# ScanApp

This software is part of prototype built for the The Open University's Module TM470.

Welcome to the BackApp hybrid app.

## How to run BackApp

Please execute the following commands:

```
cordova platform add android
cordova plugin add phonegap-nfc
```

## License

The source code is released under the Apache License v2.0

## Author

Alessandro Pieri, 2018