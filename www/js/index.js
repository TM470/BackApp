/*
 * Copyright 2018 Alessandro Pieri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    onDeviceReady: function() {
        var BACKEND_ENDPOINT = "http://10.0.0.13:8080";

        var intervalID = null;
        var intervalIDInProgress = null;
        var currentTryOnId = null;
        var currentIndex = null;
        var nfcHandler = undefined;
        var sourceShopId = "1";

        // Private functions

        /**
         * Display error in case the NFC is not enabled.
         */
        function listenerFailed() {
            displayError('Please enable NFC from the phone\'s settings');
        }

        /**
         * Print error on screen.
         * @param message Error message
         */
        function displayError(message) {
            new $.nd2Toast({
                message : message,
                action : {
                    title : "OK",
                    color : "red"
                },
                ttl : 3000
            });
        }

        /**
         * Convert a nested array to JSON string.
         * @param fromArray Array to convert
         * @returns {string} A JSON string of the array
         */
        function toJson(fromArray) {
            var jsonStr = '[';
            if (fromArray.length > 0) {
                var jsonArray = [];
                for (var i = 0; i < fromArray.length; i++) {
                    jsonArray.push(JSON.stringify(fromArray[i]));
                }
                jsonStr += jsonArray.join(',');
            }
            jsonStr += ']';
            return jsonStr;
        }

        /**
         * Intercept a NFC event (when a tag is placed in proximity of the device)
         * and write a NDEF record into that tag.
         * @param nfcEvent Detected NFC event
         */
        function onWrite(nfcEvent) {
            var mimeType = "text/scanapp-product";
            var payload = {productId: $('#productCode').val(), shopId: $('#shopId').val()}
            var record = ndef.mimeMediaRecord(mimeType, nfc.stringToBytes(JSON.stringify(payload)));

            nfc.write(
                [record],
                function () {
                    new $.nd2Toast({
                        message : 'The tag is now ready to use!',
                        ttl : 3000
                    });
                    nfcHandler = undefined;
                },
                listenerFailed
            );
        }

        /**
         * Set timer to update the INCOMING and IN_PROGRESS list.
         * @param interval Time interval in ms
         */
        function refreshList(interval) {
            intervalID = setInterval(function() { printList('incomingList', null); }, interval*1000);
            intervalIDInProgress = setInterval(function() { printList('inProgressList', 'IN_PROGRESS'); }, interval*1000);
        }

        /**
         * Display the list.
         * @param listName Name of the list to display (jquery id)
         * @param status The status to filter the list out
         */
        function printList(listName, status) {
            if (null == status) {
                path = '/try-on/?shopId=' + sourceShopId;
            } else {
                path = '/try-on/?status=' + status + '&shopId=' + sourceShopId;
            }

            $.get(BACKEND_ENDPOINT + path, function(data) {
                $('#' + listName).empty();
                for (var i in data) {
                    var productId = data[i].product.id
                    var productImage = data[i].product.photo;
                    var productBrand = data[i].product.brand;
                    var productName = data[i].product.name;
                    var productSize = data[i].sizes.join(' &middot; ');
                    var tryOnId = data[i].id;

                    var html = '<li class="nd2-card">';
                    html += '<div class="card-title has-supporting-text" style="padding-right: 0px;"><div class="ui-grid-a"><div class="ui-block-a"><h5 class="card-subtitle">PRODUCT CODE</h5><h3 class="card-primary-title">' + productId + '</h3></div><div class="ui-block-b" style="text-align: right;">';

                    if (listName == "incomingList") {
                        html += '<h5 class="card-subtitle">SIZES</h5><h3 class="card-primary-title">' + productSize + '</h3></div></div></div>';
                    } else {
                        html += '<h5 class="card-subtitle">REQUEST #</h5><h3 class="card-primary-title">' + tryOnId + '</h3></div></div></div>';
                    }
                    html += '<div class="card-media"><img src="' + BACKEND_ENDPOINT + productImage + '"></div>';
                    html += '<div class="card-supporting-text has-action"><div class="ui-grid-solo"><strong>' + productBrand + '</strong><br/> ' + productName + '</div></div>';

                    if (listName == "incomingList") {
                        html += '<div class="card-action"><div class="row between-xs"><div class="col-xs-12"><div class="box align-center"><a href="#null" rel="external" data-tryon-product="' + productId + '" data-tryon="' + tryOnId + '" data-tryon-index="' + i + '" onclick="app.confirm(this); return false;" class="ui-btn ui-btn-inline">Take in charge</a></div>';
                    } else {
                        html += '<div class="card-action"><div class="row between-xs"><div class="col-xs-12"><div class="box align-center"><a href="#" rel="external" class="ui-btn ui-btn-inline" onclick="app.printPopup(\'' + tryOnId +'\');">Details</a></div>';
                    }
                    html += '</div></div></div></li>';

                    $('#' + listName).append(html);
                }
                $('#' + listName).listview('refresh');
            }).fail(function() {
                displayError('Error retrieving data from the server.');
            });
        }

        /**
         * Bind NFC listener to list for 'text/scanapp-product' mime-type
         */
        function bindListener() {
            if (typeof nfc != "undefined") {
                nfc.addTagDiscoveredListener(nfcDispatcher, function() { }, listenerFailed);
                nfc.addMimeTypeListener('text/scanapp-product', nfcDispatcher, function() { }, listenerFailed);
            }
        }

        /**
         * Dispatch call to the bound listener.
         * @param nfcEvent NFC NDEF event
         */
        function nfcDispatcher(nfcEvent) {
            if (typeof nfcHandler != "undefined") {
                nfcHandler(nfcEvent);
            }
        }

        // Public functions

        /**
         * Take charge of a try-on request.
         * @param obj DOM object with 'data' attributes
         */
        this.confirm = function(obj) {
            currentIndex = $(obj).attr('data-tryon-index');
            currentTryOnId = $(obj).attr('data-tryon');
            var productId = $(obj).attr('data-tryon-product');

            $('#popupRequestId').html(currentTryOnId);
            $('#popupProductId').html(productId);

            $('#popupTakeCharge').popup('open'); // open popup
        };

        /**
         * Set the try-on request to 'IN_PROGRESS'.
         */
        this.tryOnConfirm = function() {
            $.ajax({
                type: 'PUT',
                url: BACKEND_ENDPOINT + '/try-on/' + currentTryOnId + '?status=IN_PROGRESS',
                contentType: "application/json",
                success: function(data) {
                    $('#tryOnList li').eq(currentIndex).remove();
                    $('#popupTakeCharge').popup('close'); // close popup
                    app.printPopup(currentTryOnId);
                },
                dataType: 'json'
            }).fail(function() {
                displayError('There was an error submitting your request.');
            });
        }

        /**
         * Disable 'DELIVERED' button in case nothing is checked in the listview.
         * @param listViewName Jquery id of the listview to check.
         */
        this.changeStatus = function(listViewName) {
            if ($('#' + listViewName + ' input:checked').length == 0) {
                //disable delivered
                $('#buttonDelivered').addClass('ui-disabled');
            } else {
                $('#buttonDelivered').removeClass('ui-disabled');
            }
        };

        /**
         * Collect the availability info a send the update to BackMs (including
         * the new status).
         * @param listViewName Listview to check.
         * @param tryOnId Try-on request ID
         * @param newStatus New status
         */
        this.sendStatusUpdate = function(listViewName, tryOnId, newStatus) {
            var result = [];
            $('#' + listViewName + ' input').each(function() {
                if ($(this).prop('checked')) {
                    result.push({ size: $(this).val(), availability: 'AVAILABLE' });
                } else {
                    result.push({ size: $(this).val(), availability: 'NOT_AVAILABLE' });
                }
            });

            $.ajax({
                type: 'PUT',
                url: BACKEND_ENDPOINT + '/try-on/' + tryOnId + '?status=' + newStatus,
                contentType: "application/json",
                success: function(data) {
                    $('#popupDialog').popup('close'); // close popup
                },
                error: function() {
                    displayError('There was an error submitting your request.');
                },
                data: toJson(result),
                dataType: 'json'
            });
        }

        /**
         * Print pop-up containing the details of a try-on request.
         * @param tryOnId ID of the try-on request
         */
        this.printPopup = function(tryOnId) {
            $.get(BACKEND_ENDPOINT + '/try-on/?id=' + tryOnId, function(data) {
                $('#popupDialog').empty();
                for (var i in data) {
                    var productId = data[i].product.id
                    var productImage = data[i].product.photo;
                    var productBrand = data[i].product.brand;
                    var productName = data[i].product.name;
                    var tryOnId = data[i].id;

                    var html = '<div class="nd2-card">';

                    html += '<div class="card-title has-supporting-text" style="padding-right: 0px;"><div class="ui-grid-a"><div class="ui-block-a"><h5 class="card-subtitle">PRODUCT CODE</h5><h3 class="card-primary-title">' + productId + '</h3></div><div class="ui-block-b" style="text-align: right;">';
                    html += '<h5 class="card-subtitle">REQUEST #</h5><h3 class="card-primary-title">' + tryOnId + '</h3></div></div></div>';
                    html += '<div class="card-media"><img src="' + BACKEND_ENDPOINT + productImage + '"></div>';
                    html += '<div class="card-supporting-text has-action" style="padding-bottom: 0px;"><div class="ui-grid-solo"><strong>' + productBrand + '</strong><br/> ' + productName + '</div><div class="ui-grid-solo"><form><fieldset data-role="controlgroup" id="popupCheckArray' + tryOnId + '"></fieldset></form></div></div>';
                    html += '<div class="card-action" style="padding-top: 0px;"><div class="ui-grid-a"><div class="ui-block-a">' +
                        '<a href="#" rel="external" onclick="app.sendStatusUpdate(\'popupCheckArray' + tryOnId + '\', \'' + tryOnId + '\', \'REJECTED\');" class="ui-btn ui-btn-inline">Reject</a></div><div class="ui-block-b" style="text-align: right;">' +
                        '<a href="#" rel="external" id="buttonDelivered" onclick="app.sendStatusUpdate(\'popupCheckArray' + tryOnId + '\', \'' + tryOnId + '\', \'DELIVERED\');" class="ui-btn ui-btn-inline">Delivered</a></div></div></div>';
                    html += '</div></div></div></div>';

                    //$('#inProgressList').listview(); //init
                    $('#popupDialog').append(html);

                    $('#popupCheckArray' + tryOnId).controlgroup(); //init
                    var sizeHtml = '';
                    for (var j = 0; j < data[i].sizes.length; j++) {
                        var size = data[i].sizes[j];
                        sizeHtml += '<label for="checkbox-size-' + i + '"><input type="checkbox" name="popup-checkbox-size-' + i + '" value="' + size + '" onclick="app.changeStatus(\'popupCheckArray' + tryOnId + '\')" checked="">' + size + '</label>';
                    }
                    $('#popupCheckArray' + tryOnId).controlgroup('container').append(sizeHtml);
                    $('#popupCheckArray' + tryOnId).enhanceWithin().controlgroup('refresh');
                    break;
                }
                //$('#inProgressList').listview('refresh');
                $('#popupDialog').popup('open'); // open popup
                //$('body').css('overflow','hidden');
            }).fail(function() {
                displayError('There was an error submitting your request.');
            });
        }

        /**
         * Enable the write listener, the one in charge to write on a detected
         * NFC tag.
         */
        this.writeTag = function() {
            nfcHandler = onWrite;
            new $.nd2Toast({
                message : 'Place the tag on the back of the phone',
                action : {
                    title : "OK",
                    color : "lime"
                },
                ttl : 5000
            });
        };

        /**
         * Set the shop ID. It will be used to poll for try-on requests.
         */
        this.writeShop = function() {
            sourceShopId = $('#sourceShopId').val();

            // go pack to the first page
            $(':mobile-pagecontainer').pagecontainer('change', '#page1', {reverse: false, changeHash: false});
        };

        bindListener();
        refreshList(5);
    }
};

app.initialize();